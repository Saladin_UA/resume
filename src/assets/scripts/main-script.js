'use strict';

var menuBasic = document.querySelector('.menu-basic');
var menuFront = document.querySelector('.menu-front');
var menuItem = document.querySelectorAll('.menu-item');

function showMenuBasic() {
    menuFront.classList.remove('active');
    menuBasic.classList.add('active');
    
    var i;
    for (i = 0; i < menuItem.length; i = i + 1) {
        menuItem[i].classList.add('animate');
    }
}

menuFront.addEventListener("click", showMenuBasic);
menuFront.innerHTML = menuFront.dataset.text;