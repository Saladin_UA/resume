'use strict';

var levels = document.querySelectorAll('.content td:not(:first-child)'),
    details = document.querySelectorAll('.details');

for (var i = 0; i < levels.length; i++) {
    var cell = levels[i];
    
    cell.innerHTML = cell.innerHTML + levels[i].dataset.level;
    
    cell.addEventListener('mouseenter', function () {
        var arrow = this.firstElementChild,
            originalColor = getComputedStyle(arrow).borderTopColor;
        arrow.style.borderTopColor = getComputedStyle(this).color;
        arrow.classList.add('hovered');
        this.addEventListener('mouseleave', function () {
            arrow.style.borderTopColor = originalColor;
            arrow.classList.remove('hovered');
        });
    });
    
    cell.addEventListener('click', function () {
        var block = this.lastElementChild;
        
        block.addEventListener('mouseleave', function () {
            this.style.opacity = '0';
            this.style.zIndex = '-5';
        });
        if (getComputedStyle(block).opacity === '0') {
            block.style.opacity = '1';
            block.style.zIndex = '15';
        } else {
            block.style.opacity = '0';
            block.style.zIndex = '-5';
        }
    });
}


