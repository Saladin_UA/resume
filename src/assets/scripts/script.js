'use strict';

var links = document.querySelectorAll('a:not(.menu-item)');
var showMenu = document.getElementById('cont-foot-link');
var contacts = document.querySelector('.contacts');

function showContacts() {
    if (contacts.style.display === 'flex') {
        contacts.style.display = 'none';
    } else {
        contacts.style.display = 'flex';
    }
}

function hideContacts() {
    contacts.style.display = 'none';
}
    

showMenu.addEventListener('click', showContacts);
contacts.addEventListener('mouseleave', hideContacts);

for (var i = 0; i < links.length; i++) {
    links[i].innerHTML = links[i].dataset.text;
}