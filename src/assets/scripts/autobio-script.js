'use strict';

var vasil = document.querySelector('.vasil'),
    semen = document.querySelector('.semen'),
    head = document.querySelector('.head'),
    arrow = document.querySelector('.arrow'),
    container = document.querySelector('.autobios');

function show() {    
    vasil.style.animationName = 'book';
    semen.style.animationName = 'book';
    arrow.style.animationName = 'hide';
    container.classList.add('done');
    head.classList.add('done');
}

head.addEventListener('click', show);