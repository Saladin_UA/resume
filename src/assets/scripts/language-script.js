'use strict';

var flagsVas = document.querySelectorAll('.vas .language:not(.name)'),
    flagsSam = document.querySelectorAll('.sam .language:not(.name)');

function switchFlagVas() {
    for (var i = 0; i < flagsVas.length; i++) {
        flagsVas[i].classList.remove('active');
    }
    this.classList.add('active');
}

function switchFlagSam() {
    for (var i = 0; i < flagsSam.length; i++) {
        flagsSam[i].classList.remove('active');
    }
    this.classList.add('active');
}

for (var i = 0; i < flagsVas.length; i++) {
    flagsVas[i].addEventListener('click', switchFlagVas);
    flagsSam[i].addEventListener('click', switchFlagSam);
}