'use strict';

var images = document.querySelectorAll('.image');
var photo = document.querySelector('.container');
var h = document.querySelector('h2');
var arrowLeft = document.querySelector('.arrow-left');
var arrowRight = document.querySelector('.arrow-right');

function setPhoto() {
    var theSwitcher = document.querySelector('.active');
    photo.style.backgroundImage = theSwitcher.dataset.url;
    h.innerHTML = theSwitcher.dataset.caption;
}

function switcher() {
    for (var i = 0; i < images.length; i++) {
        images[i].classList.remove('active');
    }
    this.classList.add('active');    
    setPhoto();
}

function rightSwitcher () {
    var currentSwitcher = document.querySelector('.active');
    var nextSwitcher = document.querySelector('.active + .image');
    
    if (currentSwitcher.nextElementSibling == null) {
        nextSwitcher = document.querySelector('.image:first-of-type');
    }
    
    currentSwitcher.classList.remove('active');
    nextSwitcher.classList.add('active');
    
    setPhoto();
}

function leftSwitcher () {
    var currentSwitcher = document.querySelector('.active');
    var previousSwitcher = currentSwitcher.previousElementSibling;
    
    if (currentSwitcher.previousElementSibling == null) {
        previousSwitcher = document.querySelector('.image:last-of-type');
    }
    
    currentSwitcher.classList.remove('active');
    previousSwitcher.classList.add('active');
    
    setPhoto();
}

for (var i = 0; i < images.length; i++) {
    images[i].addEventListener('click', switcher);
}

arrowRight.addEventListener('click', rightSwitcher);
arrowLeft.addEventListener('click', leftSwitcher);