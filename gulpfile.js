'use strict';

//Підключення залежностей з package.json
var gulp = require('gulp'),
    preFixer = require('gulp-autoprefixer'),
    unCSS = require('gulp-uncss'),
    minifyJS = require('gulp-uglify'),
    rimRaf = require('rimraf'),
    rigger = require('gulp-rigger'),
    sourceMaps = require('gulp-sourcemaps'),
    image = require('gulp-image'),
    
    watch = require('gulp-watch'),
    
    browserSync = require('browser-sync'),
    reload = browserSync.reload;

//Шляхи до файлів (на випадок редагування структури)
var path = {
    
    build: {
        html: 'build/',
        js: 'build/assets/scripts',
        css: 'build/assets/styles',
        img: 'build/assets/img',
        svg: 'build/assets/svg'
    },
    
    src: {
        html: 'src/*.html',
        js: 'src/assets/scripts/*.js',
        css: 'src/assets/styles/*.css',
        img: 'src/assets/img/*',
        svg: 'src/assets/svg/*'
    },
    
    watch: {
        html: 'src/*.html',
        js: 'src/assets/scripts/*.js',
        css: 'src/assets/styles/*.css',
        img: 'src/assets/img/*',
        svg: 'src/assets/svg/*',
        tmp: 'src/assets/templates/*'
    },
    
    clean: './build'
};

gulp.task('webserver', function () {
    browserSync({
        server: {
            baseDir: "./build"
        },
        host: 'localhost',
        port: 3000,
        tunnel: true
    });
});


//Збірка HTML
gulp.task('html:build', function () {
    gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});

//Збірка JS
gulp.task('js:build', function () {
    gulp.src(path.src.js)
        .pipe(rigger())
        .pipe(sourceMaps.init())
        .pipe(sourceMaps.write())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});

//Збірка СSS
gulp.task('css:build', function () {
    gulp.src(path.src.css)
        .pipe(rigger())
        .pipe(sourceMaps.init())
        .pipe(preFixer())
        .pipe(sourceMaps.write())
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});


//Збірка SVG
gulp.task('svg:build', function () {
    gulp.src(path.src.svg)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.svg))
        .pipe(reload({stream: true}));
});

//Збірка картинок
gulp.task('img:build', function () {
    gulp.src(path.src.img)
        .pipe(image())
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream: true}));
});
//Загальна збірка
gulp.task('build-project', [
    'html:build',
    'js:build',
    'css:build',
    'svg:build',
    'img:build'
]);

//Автоматична збірка
gulp.task('watch', function () {
    watch([path.watch.html], function (ev, callback) {
        gulp.start('html:build');
    });
    watch([path.watch.js], function (ev, callback) {
        gulp.start('js:build');
    });
    watch([path.watch.css], function (ev, callback) {
        gulp.start('css:build');
    });
    watch([path.watch.svg], function (ev, callback) {
        gulp.start('svg:build');
    });
    watch([path.watch.img], function (ev, callback) {
        gulp.start('img:build');
    });
    watch([path.watch.tmp], function (ev, callback) {
        gulp.start('html:build');
    });
});

//Очистка
gulp.task('clean', function (callback) {
    rimRaf(path.clean, callback);
});

//Запуск роботи з проектом
gulp.task('default', ['build-project', 'webserver', 'watch']);